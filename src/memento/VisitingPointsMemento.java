/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package memento;

import route.Point;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Class responsible for being the memento in the memento pattern
 * @author Henoch
 */
public class VisitingPointsMemento implements Serializable{
    private List<Point> avaiablePointsMemento;
    private  List<Point> pointsToVisitMemento;
    
    /**
     * initiates the avaiblePointsMemento to be a list with the list avaiablePoints in it
     * initiates the pointsToVisitMemento to be a list with the list pointsToVisit in it
     * @param avaiablePoints, the points that can still be choosen
     * @param pointsToVisit, the points that were already choosen 
     */
    public VisitingPointsMemento(List<Point> avaiablePoints, List<Point> pointsToVisit) {
        this.avaiablePointsMemento = new ArrayList<>(avaiablePoints);
        this.pointsToVisitMemento = new ArrayList<>(pointsToVisit);
    }

    /**
     * returns the list of avaiablePointsMemento
     * @return avaiablePointsMemento
     */
    public List<Point> getAvaiablePointsMemento() {
        return avaiablePointsMemento;
    }

    /**
     * returns the list of pointsToVisitMemento
     * @return pointsToVisitMemento 
     */
    public List<Point> getPointsToVisitMemento() {
        return pointsToVisitMemento;
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public String toString() {
        return ""+pointsToVisitMemento+ "\n" + avaiablePointsMemento;
    }
    
}

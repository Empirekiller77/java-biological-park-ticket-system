/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package memento;

import route.ErrorAlertException;
import java.io.Serializable;
import java.util.Stack;

/**
 * Class responsible for being the caretaker in the memento pattern
 * @author Henoch
 */
public class VisitingPointsCareTaker implements Serializable{
    private Stack<VisitingPointsMemento> objMementos;

    /**
     * initializes the stack
     */
    public VisitingPointsCareTaker() {
        this.objMementos = new Stack<>();
    }
    
    /**
     * adds a memento to the stack of mementos
     * @param visitingPoints a variable of VisitingPoints
     */
    public void saveState(VisitingPoints visitingPoints){
        VisitingPointsMemento objMemento = visitingPoints.createMemento();
        objMementos.push(objMemento);
    }
    
    /**
     * takes the last inserted memento out of the stack
     * Sets it to be the parameter visitingPoints Memento
     * @param visitingPoints a variable of VisitingPoints
     */
    public void restoreState(VisitingPoints visitingPoints){
        if(objMementos.isEmpty()) throw new ErrorAlertException("Não há bilhetes a retroceder");
        VisitingPointsMemento objMemento = objMementos.pop();
        visitingPoints.setMemento(objMemento);
    }
  
}

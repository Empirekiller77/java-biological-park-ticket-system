/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package memento;

import route.Point;
import java.io.Serializable;
import java.util.List;

/**
 * Class responsible for aggregating the interest points that are being visited and the ones that yet to be visited
 * @author Henoch
 */
public class VisitingPoints implements Serializable{
    private List<Point> avaiablePoints;
    private  List<Point> pointsToVisit;
    
    /**
     * instanciates visitingpoints variable 
     * @param avaiablePoints the points that can still be choosen
     * @param pointsToVisit the points that were already choosen
     */
    public VisitingPoints(List<Point> avaiablePoints, List<Point> pointsToVisit) {
        this.avaiablePoints = avaiablePoints;
        this.pointsToVisit = pointsToVisit;
    }

    /**
     * returns a new VisitingPointsMemento
     * @return new VisitingPointsMemento
     */
    public VisitingPointsMemento createMemento(){
        return new VisitingPointsMemento(avaiablePoints, pointsToVisit);
    }
    
    /**
     * sets the avaiablePoints to be the avaiablePoints of the memento
     * sets the pointsToVisit to be the pointsToVisit of the memento
     * @param memento 
     */
    public void setMemento(VisitingPointsMemento memento){
        avaiablePoints = memento.getAvaiablePointsMemento();
        pointsToVisit = memento.getPointsToVisitMemento();
    }
    
    /**
     * returns the avaiablePoints
     * @return avaiablePoints
     */
    public List<Point> getAvaiablePoints() {
        return avaiablePoints;
    }
    
    /**
     * returns the pointsToVisit
     * @return pointsToVisit
     */
    public List<Point> getPointsToVisit() {
        return pointsToVisit;
    }
    
    
    
    
    
   
}

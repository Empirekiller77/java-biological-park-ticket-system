/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import stats.GraphStats;
import Singleton.ActionLoggerSingleton;
import templateMethod.GeneratePDF;
import memento.VisitingPoints;
import memento.VisitingPointsCareTaker;
import com.itextpdf.text.DocumentException;
import dao.ChoosePathAppDAO;
import dao.ChoosePathAppDAOSerialization;
import graph.Edge;
import graph.Graph;
import graph.Vertex;
import templateMethod.Bill;
import route.Connection;
import route.ErrorAlertException;
import route.Point;
import route.RouteManager;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Separator;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import graphview.CircularSortedPlacementStrategy;
import graphview.GraphPanel;
import graphview.VertexPlacementStrategy;
import stats.Stats;

/**
 * Responsible for the main logic of the application. Including, maneging all interations with the user and method calling
 * @author Lima and Henoch
 */
public class ChoosePathApp extends VBox implements Serializable{
    private HBox hboxTittle, hboxMiddle, hboxFooter, hboxRadio, hboxRadioCriteria, hboxPathCost, hboxPathDistance, hboxTicketButtons; 
    private VBox vboxRightMiddle, vboxLeftMiddle, 
            vboxRightFooter, vboxLeftFooter;
    private Label lblTittle, lbCost,lbDistance, lbCriteria, lblTicket, lblCostResult, lblDistanceResult;
    private CheckBox taxPayerCb;
    private Button btnViewMap,btnTicket, btnUndo, btnPointToVisit, btnPointUnvisit, btnStats, btnSaveInfo;

    
    private ObservableList<Point> pointList;
    private ObservableList<Point> pointsToVisitList;
    private ListView<Point> pointsView, pointsViewPath;
    private List<Connection> travelledPaths;
    
    private ToggleGroup radioGroupPathType, radioGroupCriteria;
    private RadioButton rbFoot, rbBycicle, rbCost, rbDistance;
    //cost and distance
    private double currentCost, distance;
    
    //stats
    private HashMap<Point, Integer> pointsStats;
    private Stats stats;
    
    //Properties file
    private Properties properties;
    
    //PointLists Memento
    private VisitingPointsCareTaker visitCaretaker;
    private VisitingPoints visitingPoints;
    private Label lblRadio;
    
    private String tripType;
    
    //logger
    ActionLoggerSingleton logger;
            
    /**returns a VBox with all the main components of the app
     *
     * @param rManager - A route manager with the loaded map
     * @param prop - Properties file with the info about with info to log
     * @param visitCaretaker - the caretaker of the visitingPointsMenento, for the Undo option of the tickets
     * @param stats - An object of the statistics of sold tickets
     * @param mostVisitedPoints - An hashmap of points with the value being the number of visits of each
     */
    public ChoosePathApp(RouteManager rManager, Properties prop, VisitingPointsCareTaker visitCaretaker, 
            Stats stats, HashMap<Point, Integer> mostVisitedPoints){
        
        initiateMainComponents(rManager, prop, visitCaretaker, stats, mostVisitedPoints);
 
        //tittle
        this.hboxTittle = new HBox();
        hboxTittle.setPadding(new Insets(10));
        hboxTittle.setSpacing(10);
        
        //add tittle to hboxtittle
        this.lblTittle = new Label("Selecionar Trajeto");
        hboxTittle.getChildren().addAll(lblTittle);
        
        //BTNS
        this.btnViewMap = visualizeMap(rManager);
        this.btnTicket = TicketBtn(rManager);
        this.btnUndo = undoBtn();
        this.btnPointToVisit = addPointToVisitBtn();
        this.btnPointUnvisit = removePointToVisitBtn();
        this.btnStats = btnStats();
        this.btnSaveInfo = saveInfoBtn();
  
        //hbox criteria
        this.lbCriteria = new Label("Selecione o Critério a Minimizar:");
        this.hboxRadioCriteria = new HBox();
        
        hboxMiddlePosition();
        costAndDistHbox();
        bikeFootRadios();
        
        buildFooter();
        //this VBOX
        this.setSpacing(10);
        this.setPadding(new Insets(10));
        this.setAlignment(Pos.CENTER);
        this.getChildren().addAll(hboxTittle, hboxMiddle, hboxFooter);
    }
    
   /**
    * Fills the observable list with the points from the graph and resets the cost and distance labels
    * @param rManager the route manager to take the points from
    */
    private void resetPointListsAndLabels(RouteManager rManager){
        pointsToVisitList.clear();
        pointList.clear();

        if(rManager.getDiGraph().numVertices()<1){
            
        }
        Graph<Point, Connection> g = rManager.getDiGraph();
        for (Vertex<Point> p : g.vertices()) {
            pointList.add(p.element());
        }
        
         for (Point p : pointList) {
            if(p.getName().equalsIgnoreCase("Entrada")){
                 pointsToVisitList.add(p);
                 pointList.remove(p);
                 break;
            }
            
        }
         //RESET COST AND DISTANCE LABELS
         if(this.lblCostResult != null && this.lblDistanceResult != null){
             this.lblCostResult.setText("  0");
             this.lblDistanceResult.setText("  0");
             
         }
         
        
    }
    
    /**
     * Returns the button that emmits a ticket and its bill
     * @param rmanager the route manager to wich we need to refill the list of point with
     * @return the emmiting ticket button
     */
    private Button TicketBtn(RouteManager rmanager){
        Button ticketBtn = new Button("Emitir");
        
        ticketBtn.setOnAction(e -> {
            try{
                if(this.currentCost == -1 || this.distance == -1){
                    throw new ErrorAlertException("Simule um caminho!");
                }
                
                if(this.taxPayerCb.isSelected()){
                  
                    Stage stage = new InsertTaxPayerNumberWindow(this.currentCost, this.distance, this.pointsToVisitList, this.travelledPaths, this.tripType);
                    
                }else{
                    GeneratePDF.GeneratePDF(Bill.semContribuiente(this.currentCost, this.distance, this.pointsToVisitList, this.travelledPaths, this.tripType));
                }
                
                //memento
                this.visitingPoints = new VisitingPoints(this.pointList, this.pointsToVisitList);
                this.visitCaretaker.saveState(this.visitingPoints);
                
                
                for(Point p : this.pointsToVisitList){
                    for(Point pHash : pointsStats.keySet()){
                        if(p == pHash)
                            pointsStats.put(p, pointsStats.get(p)+1);
                    }
                }
                
                if(this.rbBycicle.isSelected()){
                    this.stats.sellBikeTicket(this.currentCost);
                }else{
                    this.stats.sellFootTicket(this.currentCost);
                }

                if(this.properties.getProperty("Logging").equals("Tickets")){
                     String footBike;
                        if(this.rbBycicle.isSelected()){
                            footBike = "Bicicleta";
                        }else{
                            footBike = "A Pé";  
                        }
                    logger.log("Um bilhete foi emitido para: " + footBike + " com o custo de " + this.currentCost + " e a distância de " + this.distance);
                }
                
                    this.currentCost = -1;
                    this.distance = -1;
                    
                    //MOSTRAR ALERT
                    showMessage("Bilhete Emitido com Sucesso!");
                    //RESETAR LISTAS
                    resetPointListsAndLabels(rmanager);
                    //RESETAR TF
            }catch(ErrorAlertException exp){
                
            } catch (FileNotFoundException ex) {
                Logger.getLogger(ChoosePathApp.class.getName()).log(Level.SEVERE, null, ex);
            } catch (DocumentException | IOException ex) {
                Logger.getLogger(ChoosePathApp.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        });
        
        return ticketBtn;
    }
    
    /**
     * Creates teh button that adds a chosen point to the chosen points list
     * @return the button to add points to the visiting list
     */
    private Button addPointToVisitBtn(){
        Button visitBtn = new Button("Adicionar Ponto de Interesse");
        
        visitBtn.setOnAction(e -> {
            if(pointsView.getSelectionModel().getSelectedItem() == null){
                throw new ErrorAlertException("Tem de selecionar um tempo para inserir!");
            }
            pointsToVisitList.add(pointsView.getSelectionModel().getSelectedItem());
            pointList.remove(pointsView.getSelectionModel().getSelectedItem());
        });
        
        return visitBtn;
    }
    
    /**
     * creates a button that, on action, remove the button from the visiting points, to the aviable points
     * @return the removing point button 
     */
    private Button removePointToVisitBtn(){
        Button visitBtn = new Button("Remover Ponto de Interesse");
        
        visitBtn.setOnAction(e -> {
            if(pointsViewPath.getSelectionModel().getSelectedItem().getName().equalsIgnoreCase("Entrada")){
                throw new ErrorAlertException("Precisa sempre passar na entrada!");
            }
            
            pointList.add(pointsViewPath.getSelectionModel().getSelectedItem());
            pointsToVisitList.remove(pointsViewPath.getSelectionModel().getSelectedItem());
        });
        
        return visitBtn;
    }
    
    /**
     * Creates the button that, on action presetns the visual map with the simulated path
     * @param rmanager - the route manager wich we get the graph from
     * @return the visualizing button
     */
    private Button visualizeMap(RouteManager rmanager){
        Button visualizeMap = new Button("Simular Percurso");
        
        visualizeMap.setOnAction(e -> {
            try{
                drawGraph(rmanager);
            }catch(ErrorAlertException exp){
                resetPointListsAndLabels(rmanager);
            } catch (IOException ex) {
                Logger.getLogger(ChoosePathApp.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        });
        
        return visualizeMap;
    }
    
    /**
     * Crated the button to consult the ticket statistic
     * @return the statistic consulting button
     */
    private Button btnStats(){
        Button btnStats = new Button("Ver Estatisticas");
        
        btnStats.setOnAction(e ->{
            try{
                
                if(this.stats.getnTotalSold() == 0)
                    throw new ErrorAlertException("Não existem dados suficientes para apresentar uma estatistica!");
                
                Stage stage = new GraphStats(this.pointsStats, this.stats);
                if(this.properties.getProperty("Logging").equals("Stats")){
                    logger.log("As estatísticas foram consultadas");
                }
                
            }catch(ErrorAlertException exp){
                
            } catch (IOException ex) {
                Logger.getLogger(ChoosePathApp.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        
        return btnStats;
    }
    
    /**
     * Creates the button to undo the ticket configuration, using the memento pattern
     * @return the undo button
     */
    private Button undoBtn(){
       Button btnUndo = new Button ("Retroceder");
        
        btnUndo.setOnAction(e -> {
            
                this.visitCaretaker.restoreState(this.visitingPoints);
                this.pointList.clear();
                this.pointsToVisitList.clear();
                this.pointList.addAll(this.visitingPoints.getAvaiablePoints());
                this.pointsToVisitList.addAll(this.visitingPoints.getPointsToVisit());  
                showMessage("Retrocedeu para a última Configuração de Bilhete!");
            
        });
        
        return btnUndo;
    }
    
    /**
     * Draws the visual graph with the highlighted path and points the path goes through
     * @param rmanager the route manager of weich we get the graph/map from
     * @throws IOException the exception catched in the visualizeMap button
     */
    private void drawGraph(RouteManager rmanager) throws IOException{
        
        List <Point> pointsToGet = new ArrayList<>();
        this.travelledPaths.clear();
        
        if(this.pointsToVisitList.size() > 1){
            
                this.lblCostResult.setText("  0");
                this.lblDistanceResult.setText("  0");
                double objectiveCriteria = 0;
                double secondaryCriteria = 0;
                RouteManager.Criteria criteria;
                Label tf;
                Label secondtf;
                boolean isBycicle;
                  
                if(this.rbCost.isSelected()){
                    criteria = RouteManager.Criteria.COST;
                    tf = this.lblCostResult;
                    secondtf = this.lblDistanceResult;
                    
                }else{
                    criteria = RouteManager.Criteria.DISTANCE;
                    tf = this.lblDistanceResult;
                    secondtf = this.lblCostResult;
                }
                
                
                if(this.rbBycicle.isSelected()){
                    isBycicle = true;
                }else{
                    isBycicle = false;
                }
                
                
                
                try{
                                    
                    objectiveCriteria = rmanager.calcRoute(pointsToVisitList, criteria, pointsToGet, travelledPaths, isBycicle);
  
                    VertexPlacementStrategy strategy = new CircularSortedPlacementStrategy();
                    
                    GraphPanel<Point, Connection> graphView = new GraphPanel<>(rmanager.getDiGraph(), strategy);
                    
                    Scene scene = new Scene(graphView, 850, 850);
                    
                    
                    //!!!IMPORTANT - Called after scene is displayed so we can have width and height values
                    graphView.plotGraph();
                    
                    //COLOR VERTEX
                    colorPathsAndVertex(rmanager, graphView);
                    
                    secondaryCriteria = getSecondCriteria(rmanager, criteria);
                      
                    
                    tf.setText(String.valueOf(objectiveCriteria));
                    secondtf.setText(String.valueOf(secondaryCriteria));
                    
                    if(this.rbCost.isSelected()){
                        this.currentCost = objectiveCriteria;
                        this.distance = secondaryCriteria;
                    }else{
                        this.distance = objectiveCriteria;
                        this.currentCost = secondaryCriteria;
                    }
                    
                    Stage stage = new Stage(StageStyle.DECORATED);
                    
                    stage.setTitle("Path View!");
                    stage.setScene(scene);
                    stage.initModality(Modality.APPLICATION_MODAL);
                    stage.showAndWait();               
                    
                    if(this.rbBycicle.isSelected()){
                        this.tripType = "Bicicleta";
                    }else{
                        this.tripType = "A pé";
                    }
                    if(this.properties.getProperty("Logging").equals("Route")){
                       String footBike;
                        if(this.rbBycicle.isSelected()){
                            footBike = "Bicicleta";
                        }else{
                            footBike = "A Pé";  
                        }
                        logger.log("Um percurso foi simulado para: "+ footBike + " com o custo de " 
                                + this.currentCost + " e a distância de " + this.distance);
                    }
                    
                }catch(ErrorAlertException exp){
                    this.currentCost = -1;
                    this.distance = -1;
                    resetPointListsAndLabels(rmanager);
                }
                
              
            }else{
                throw new ErrorAlertException("Insira pelo menos 1 ponto!");
            }    
    }

    /**
     *gets the VisitingPointsCaretaker
     * @return the VisitingPointsCaretaker
     */
    public VisitingPointsCareTaker getVisitsCaretaker(){
        return this.visitCaretaker;
    }
    
    /**
     *gets the statistic of the tickets
     * @return the current ticket statistics
     */
    public Stats getStats(){
        return this.stats;
    }
    
    /**
     * gets the current hashmap of Points and their visits
     * @return the current point visit hashmap
     */
    public HashMap<Point, Integer> getMostVisitedPoints(){
        return this.pointsStats;
    }
    
    /**
     * Shows an alert with the chosen message
     * @param message the message to be shown
     */
    public void showMessage(String message){
        Alert alert;
        alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Erro!");
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.showAndWait();
    }
    
    /**
     * Creates the button that serializes wht statistics information and the careataker
     * @return the serializing button
     */
    private Button saveInfoBtn(){
        
        Button btnSave = new Button ("Guardar Informação");
        
        btnSave.setOnAction(e -> {
            ChoosePathAppDAO chooseDAO;
            chooseDAO = new ChoosePathAppDAOSerialization();
            try{
            chooseDAO.saveChoosePathApp(this);
            showMessage("Informação da Bilheteira Guardada!");
            }catch(Exception ex){
                
            }
        });
        
        return btnSave;
    }
    
    /**
     * Colors the paths and vertex passed by, in the route
     * 
     */
    private void colorPathsAndVertex(RouteManager rmanager, GraphPanel<Point, Connection> graphView){
        Iterable<Edge<Connection, Point>> iterableEdges = rmanager.getDiGraph().edges();
        for(Edge<Connection, Point> p : iterableEdges){
            for(Connection con  : this.travelledPaths){
                if(p.element() == con){
                    
                    graphView.setEdgeColor(con, Color.DARKRED, 0.8);
                    for(Vertex<Point> pp : p.vertices()){
                        graphView.setVertexColor(pp.element(), Color.CORNFLOWERBLUE, Color.BROWN);
                    }
                }
            }
        }
    }
    
    /**
     * returns the value of the secondary criteria in question (Cost or distance)
     * @return value of the secondary criteria
     */
    private double getSecondCriteria(RouteManager rmanager, RouteManager.Criteria criteria){
        
        double secondcrit = 0;
        
        Iterable<Edge<Connection, Point>> iterableEdges = rmanager.getDiGraph().edges();
        for(Edge<Connection, Point> p : iterableEdges){
            for(Connection con  : this.travelledPaths){
                if(p.element() == con){
                    
                    if(criteria == RouteManager.Criteria.COST){
                        secondcrit += con.getDistance();
                    }else{
                        secondcrit += con.getCost();
                    }
                }
            }
        }
        
        return secondcrit;
    }
    
    private void hboxMiddlePosition(){
        hboxRadioCriteria.setPadding(new Insets(10));
        hboxRadioCriteria.setSpacing(10);
        radioGroupCriteria = new ToggleGroup();
        rbCost = new RadioButton("Custo");
        rbCost.setToggleGroup(radioGroupCriteria);
        rbCost.setSelected(true);
        rbDistance = new RadioButton("Distância");
        rbDistance.setToggleGroup(radioGroupCriteria);
        this.hboxRadioCriteria.getChildren().addAll(rbCost, rbDistance);
        
        lbCost = new Label("Cost: ");
        lbCost.paddingProperty().set(new Insets(0, 22, 0, 0));
        lbDistance = new Label("Distance: ");
        
        this.vboxRightMiddle = new VBox();
        vboxRightMiddle.setPadding(new Insets(10));
        vboxRightMiddle.setSpacing(10);
        this.vboxRightMiddle.getChildren().addAll(pointsViewPath, btnViewMap, btnPointUnvisit);
        
        //middle of the window left side
        this.vboxLeftMiddle = new VBox();
        vboxLeftMiddle.setPadding(new Insets(10));
        vboxLeftMiddle.setSpacing(10);
        this.vboxLeftMiddle.getChildren().addAll(pointsView, btnPointToVisit);
        
        //Middle of the window
        this.hboxMiddle = new HBox();
        hboxMiddle.setPadding(new Insets(10));
        hboxMiddle.setSpacing(10);
        this.hboxMiddle.getChildren().addAll(
                this.vboxLeftMiddle, 
                this.vboxRightMiddle);
    }
    
    private void costAndDistHbox(){
        //cost and path hbox
        hboxPathCost = new HBox();
        hboxPathDistance = new HBox();
        
        lblCostResult = new Label("  0");
        lblDistanceResult = new Label("  0");
        
        hboxPathCost.getChildren().addAll(lbCost,lblCostResult);

        hboxPathCost.setSpacing(10);
        hboxPathCost.setPadding(new Insets(10, 0, 0, 0));
        hboxPathDistance.getChildren().addAll(lbDistance,lblDistanceResult);
        hboxPathDistance.setSpacing(10);
    }
    
    private void bikeFootRadios(){
        //radiobtn hbox
        this.hboxRadio = new HBox();
        hboxRadio.setPadding(new Insets(10));
        hboxRadio.setSpacing(10);
        radioGroupPathType = new ToggleGroup();
        rbFoot = new RadioButton("A pé");
        rbFoot.setToggleGroup(radioGroupPathType);
        rbFoot.setSelected(true);
        this.lblRadio = new Label("Escolha o tipo de percurso:");
        this.lblRadio.setPadding(new Insets(-13 , 0 ,0, 0));
        rbBycicle = new RadioButton("Bicicleta");
        rbBycicle.setToggleGroup(radioGroupPathType);
        this.hboxRadio.getChildren().addAll(rbFoot, rbBycicle);
    }
    
    private void initiateMainComponents(RouteManager rManager, Properties prop, VisitingPointsCareTaker visitCaretaker, 
            Stats stats, HashMap<Point, Integer> mostVisitedPoints){
        //instanciate logger
        logger = ActionLoggerSingleton.getInstance();
        //check if the application is a new one, and not a saved one
        if(visitCaretaker == null){
            this.visitCaretaker = new VisitingPointsCareTaker();
        }else{
            this.visitCaretaker =  visitCaretaker;
        }
        
        if(stats == null){
              this.stats = new Stats();
        }else{
            this.stats =  stats;
        }
        //end checking for saved
        
        this.tripType = "";
        
        //properties
        this.properties = prop;

        this.currentCost = -1;
        this.distance = -1;
          //LIST VIEW
        this.pointsView = new ListView<>();  
        this.pointsViewPath = new ListView<>();
        this.pointsView.setMinHeight(50);
        this.pointsViewPath.setMinHeight(50);
        //Observabale list
        pointList = FXCollections.observableArrayList();
        pointsToVisitList = FXCollections.observableArrayList();
        this.travelledPaths = new ArrayList<>();
        
        pointsView.setItems(pointList);//definir items da list view como os da OBLIST
        pointsViewPath.setItems(pointsToVisitList);//definir items da list view como os da OBLIST de pontos a visitar
        resetPointListsAndLabels(rManager);
        
        
        //visitedPoints HashMap, checking if the app is a loaded one
        if(mostVisitedPoints == null){
            this.pointsStats = new HashMap();
            pointsStats = new HashMap<>();
            
            //if it is a new hashmap
            for(Point p : pointList){
                pointsStats.put(p, 0);
            }
        }else{
            this.pointsStats =  mostVisitedPoints;
        }
        
        this.visitingPoints = new VisitingPoints(this.pointList,pointsToVisitList);
    }
    
    private void buildFooter(){
        //footer right side
        this.vboxRightFooter = new VBox();
        vboxRightFooter.setPadding(new Insets(10));
        vboxRightFooter.setSpacing(10);
        this.vboxRightFooter.getChildren().addAll(lbCriteria, hboxRadioCriteria, btnStats, hboxPathCost, hboxPathDistance);
        this.vboxRightFooter.setPadding(new Insets(0, 0, 0, 50));
      
        //footer left side
        this.lblTicket = new Label("Emitir Bilhete: ");
        this.lblTicket.setFont(Font.font("Arial", 15));
        this.lblTicket.setStyle(" font-weight: bold");
        this.taxPayerCb = new CheckBox("Com Nº Contribuinte");
        
        this.vboxLeftFooter = new VBox();
        vboxLeftFooter.setPadding(new Insets(10));
        vboxLeftFooter.setSpacing(10);
        this.hboxTicketButtons = new HBox();
        this.hboxTicketButtons.getChildren().addAll(btnUndo, btnTicket);
        this.hboxTicketButtons.setPadding(new Insets(10, 10, 10, 0));
        btnUndo.setMinWidth(100);
        btnTicket.setMinWidth(100);
        this.hboxTicketButtons.setSpacing(10);
        this.hboxTicketButtons.setAlignment(Pos.CENTER);
      
        this.vboxLeftFooter.getChildren().addAll(this.lblRadio, hboxRadio, new Separator(),  this.lblTicket, this.taxPayerCb , this.hboxTicketButtons, this.btnSaveInfo);
        this.vboxLeftFooter.setAlignment(Pos.BASELINE_LEFT);
        
        //footer
        this.hboxFooter = new HBox();
        hboxFooter.setPadding(new Insets(10));
        hboxFooter.setSpacing(10);
        Separator footerSeparator = new Separator();
        footerSeparator.setOrientation(Orientation.VERTICAL);
        this.hboxFooter.getChildren().addAll(vboxLeftFooter, footerSeparator , vboxRightFooter);
        this.hboxFooter.setAlignment(Pos.CENTER);
        this.hboxFooter.setPadding(new Insets(10));
    }
}

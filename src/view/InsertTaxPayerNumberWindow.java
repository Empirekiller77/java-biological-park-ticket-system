/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import templateMethod.GeneratePDF;
import com.itextpdf.text.DocumentException;
import templateMethod.Bill;
import route.Connection;
import route.ErrorAlertException;
import route.Point;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * Class responsible for creating the window to insert the tax payer number on a bill
 * @author Henoch
 */
public class InsertTaxPayerNumberWindow extends Stage{
    private VBox contentVbox;
    private Label numberlbl;
    private TextField numbertf;
    private Button confirmBtn;
    private HBox topHbox;
    private double cost, distance;
    private List<Point> visitedPoints;
    private List<Connection> travelledPaths;
    private final String tripType;
    
    /**
     * returns a new InsertTaxPayerNumberWindow
     * @param cost the current ticket cost
     * @param distance the current ticket distance
     * @param visitedPoints the current ticket visited points
     * @param travelledPaths the current ticket travelled paths
     * @param tripType -  the type of trip, foot or bike
     */
    public InsertTaxPayerNumberWindow(double cost, double distance, List<Point> visitedPoints, List<Connection> travelledPaths, String tripType) {
        this.contentVbox = new VBox();
        this.numberlbl = new Label ("Nº Contribuinte: ");
        this.numbertf = new TextField();
        this.confirmBtn = confirmBtn();
        this.topHbox = new HBox();
        this.cost = cost;
        this.distance = distance;
        this.visitedPoints = visitedPoints;
        this.travelledPaths = travelledPaths;
        this.tripType = tripType;
        
        //ALLOW ONLY NUMBERS IN TEXTFIELD
        this.numbertf.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue,
                    String newValue) {
                if (!newValue.matches("\\d*")) {
                     numbertf.setText(newValue.replaceAll("[^\\d]", ""));
                }
            }
        });
        //END ALLOW ONLY NUMBERS IN TEXTFIELD
        
        topHbox.getChildren().addAll(numberlbl, numbertf);
        this.numberlbl.setPadding(new Insets(5, 10, 0 , 0));
        this.topHbox.setPadding(new Insets(15));
        this.contentVbox.setAlignment(Pos.CENTER);
        this.contentVbox.setPadding(new Insets(20));
        this.contentVbox.getChildren().addAll(topHbox, confirmBtn);
        
        Scene scene = new Scene(this.contentVbox);
        
        this.setScene(scene);
        this.initModality(Modality.APPLICATION_MODAL);
        this.showAndWait();
    }
    
    /**
     * returns the button to confirm the tax payer nubmer inserted
     * @return confirmBtn - the confirmation button
     */
    private Button confirmBtn(){
        Button confirmBtn = new Button("Confirmar");
        
        confirmBtn.setOnAction(e -> {
            try{
                if(this.numbertf.getText().length() != 9)
                    throw new ErrorAlertException("Número de Contribuinte Inválido!");
                
                GeneratePDF.GeneratePDF(Bill.comContribuiente(this.numbertf.getText(), this.cost, this.distance, this.visitedPoints, this.travelledPaths, this.tripType));
                this.close();
            }catch(ErrorAlertException exp){
                this.numbertf.clear();
            } catch (FileNotFoundException | DocumentException ex) {
                Logger.getLogger(InsertTaxPayerNumberWindow.class.getName()).log(Level.SEVERE, null, ex);
            }
         
        });
        
        return confirmBtn;
    }
}

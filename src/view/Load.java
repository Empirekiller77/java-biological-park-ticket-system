/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import dao.ChoosePathAppDAO;
import dao.ChoosePathAppDAOSerialization;
import route.ErrorAlertException;
import route.RouteManager;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
 

/**
 * Class responsible for creating the window of Load
 * @author Lima
 */
public class Load extends Stage {
    private HBox hboxInput, hboxButton;
    private VBox vboxLeftInput, vboxMiddleInput,
                vboxLeftButton, vboxContainer;
    private ComboBox<String> jsBtn;
    private Label lblJsbtn;
    private Button btnContinue;
    private ChoosePathAppDAO chooseDAO;
    private Properties prop;
    private RouteManager planner;
    
    /**
     * returns a new Load stage
     */
    public Load(){
        this.chooseDAO = new ChoosePathAppDAOSerialization();
        this.planner = null;
        this.hboxInput = new HBox();
        hboxInput.setPadding(new Insets(10));
        hboxInput.setSpacing(10);
        this.lblJsbtn = new Label("Deseja carregar informação");
        this.jsBtn = new ComboBox();
        this.jsBtn.getItems().addAll("Sim", "Não");
        this.btnContinue = Continue();
        
        this.prop = null;
        
        this.vboxLeftInput = new VBox();
        this.vboxLeftInput.getChildren().addAll(lblJsbtn);
        this.vboxMiddleInput = new VBox();
        this.vboxMiddleInput.getChildren().addAll(jsBtn);
        this.vboxLeftButton = new VBox();
        this.vboxLeftButton.getChildren().addAll(btnContinue);
        this.hboxInput.getChildren().addAll(vboxLeftInput, vboxMiddleInput);
        this.hboxButton = new HBox();
        this.hboxButton.getChildren().addAll(vboxLeftButton);
        
        this.vboxContainer = new VBox();
        this.vboxContainer.setSpacing(10);
        this.vboxContainer.setPadding(new Insets(10));
        this.vboxContainer.setAlignment(Pos.CENTER);
        this.vboxContainer.getChildren().addAll(hboxInput, hboxButton);
        this.setScene(new Scene(this.vboxContainer));
        this.show();
    }
    
    /**
     * Creates the confirmation button that:
     * Verifies wich option was chosen from the comboBox. If the option "Sim" was chosen, 
     * it loads a ChoosePathApp with the information saved in the default file
     * If the option "Não" was chosen, it loads a new ChoosePathApp, without the saved information
     * 
     * @return btnContinue confirmation button
     */
    public Button Continue(){
        Button btnContinue = new Button("Continuar");
        Stage stage = new Stage();
        
        btnContinue.setOnAction(e->{
            try {
                ChoosePathApp cpa = null;
                
                this.prop = new Properties();
                InputStream input = null;    
                input = new FileInputStream("src/properties.properties");
                this.prop.load(input);
                
                this.planner = new RouteManager();
                this.planner.loadMap("src/"+prop.getProperty("Map"));
                if(this.jsBtn.getSelectionModel().getSelectedItem() == null){
                    throw new ErrorAlertException("Selectione uma opção!");
                }
                if(this.jsBtn.getSelectionModel().getSelectedItem().equals("Sim")){
                    cpa = this.chooseDAO.loadChoosePathApp();
                    
                }else if(this.jsBtn.getSelectionModel().getSelectedItem().equals("Não")){
                    cpa = new ChoosePathApp(planner, prop, null, null, null);
                }
                
                Scene scene = new Scene(cpa);
                stage.setTitle("Bilheteira");
                stage.setScene(scene);
                
                stage.getIcons().add(new Image("/icons/park.png"));
                stage.show();
                stage.setResizable(false);
                this.hide();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(Load.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(Load.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        
        return btnContinue;
    }
}

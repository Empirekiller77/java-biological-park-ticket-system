/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package route;


import Strategy.StrategyCalcFoot;
import Strategy.StrategyCalcRouteBike;
import graph.DiGraphEdgeList;
import graph.Edge;
import graph.Graph;
import graph.InvalidVertexException;
import graph.Vertex;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import route.Connection.TYPE;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javafx.collections.ObservableList;


/**
 * Class that manages the digraph, the map and calculates the paths
 * @author Henoch Vitureira and Pedro Lima
 */
public class RouteManager {
    /**
     * Enumerate the criteria that we can search a path
     */
    public enum Criteria {
        DISTANCE, 
        COST;

        /**
         *
         * @return
         */
        public String getUnit() {
            switch(this) {
                case COST: return "€";
                case DISTANCE: return "Metros";
            }
            return "Unknown";
        }
    };
    
    private final Graph<Point, Connection> diGraph;

    /**
     * Inicialize of diGraph
     */
    public RouteManager() {
        this.diGraph = new DiGraphEdgeList<>();
    }
    
    /**
     * Gets the variable diGraph
     * 
     * @return diGraph
     */
    public Graph getDiGraph(){
        return this.diGraph;
    }
    
    /**
     * Checks if the Point belongs to this graph
     * 
     * @param p
     * @throws RoutePlannerException
     * @return the point if it exists
     */
    private Vertex<Point> checkPoint(Point p) throws RoutePlannerException {
        if( p == null) throw new RoutePlannerException("Ponto cannot be null");
        
        Vertex<Point> find = null;
        for (Vertex<Point> v : diGraph.vertices()) {
            if( v.element().equals(p)) { 
                find = v;
            }
        }
        
        if( find == null) 
            throw new RoutePlannerException("Airport with code (" + p.getId() + ") does not exist");
        
        return find;
    }
    
    /**
     * Add point to the graph
     * 
     * @param p
     * @throws RoutePlannerException
     */
    public void addPoint(Point p) throws RoutePlannerException{
        
        if( p == null ) throw new RoutePlannerException("Airport cannot be null");
        
        try {
            diGraph.insertVertex(p);
        } catch (InvalidVertexException e) {
            throw new RoutePlannerException("Airport with code (" + p.getId() + ") already exists");
            
        }
    }

    /**
     * Adds connection to the graph
     * 
     * @param p1
     * @param p2
     * @param connection
     * @throws RoutePlannerException
     */
    public void addConnection(int p1, int p2, Connection connection) 
        throws RoutePlannerException{
        
        if( connection == null) throw new RoutePlannerException("Connection is null");
        
        Vertex<Point> a1 = getPointById(p1);
        Vertex<Point> a2 = getPointById(p2);
        
        try {
            if(connection.getType() == TYPE.BRIDGE){
                diGraph.insertEdge(a1, a2, connection, true);
            }else{
                diGraph.insertEdge(a1, a2, connection, false);
            }
     
        } catch (InvalidVertexException e) {
            throw new RoutePlannerException("The connection (" + connection.getName() + ") already exists");
        }
    }
    
    /**
     * Gets the connections between two points
     * 
     * @param point1
     * @param point2
     * @return the list of connections
     * @throws RoutePlannerException
     */
    public List<Connection> getConnectionsBetween(Point point1, Point point2) 
        throws RoutePlannerException {
        Vertex<Point> v1 = checkPoint(point1);
        Vertex<Point> v2 = checkPoint(point2);
        List<Connection> connections = new ArrayList<>();
        
        if(diGraph.areAdjacent(v1, v2)){
            for(Edge<Connection,Point> e1 : diGraph.accedentEdges(v1)){
                
                if(e1.element().getType() == TYPE.BRIDGE){
                    for(Edge<Connection,Point> e2 : diGraph.incidentEdges(v2)){
                        if(e1 == e2){
                            connections.add(e1.element());
                        }
                    }
                }else{
                    for(Edge<Connection,Point> e2 : diGraph.accedentEdges(v2)){
                        if(e1 == e2){
                            connections.add(e1.element());
                        }
                    }
                }
            }
        }
        return connections;
    }
    
    /**
     * Read the map and insert values into the graph
     * 
     * @param file
     * @throws IOException
     */
    public void loadMap(String file) throws IOException{
    //CARREGAR FICHEIRO PARA UMA LISTA DE LINHAS
    Stream<String> stream = Files.lines(Paths.get(file)) ;
    List<String> lines = stream.collect(Collectors.toList());
    lines = lines.stream().map(String :: trim).collect(Collectors.toList());
    
    //SUBSTITUIR AS LINHAS DA LISTA POR LINHAS SEM ESPAÇOS
    for(String str : lines){
        String newStr = str.replaceAll("\\s","");
        lines.set(lines.indexOf(str), newStr);
    }
    
    //CRIAR UMA LISTA DE LISTAS DE LINHAS
    List <List<String>> listSplittedLines = new ArrayList<>();
    
    
    //PREENCHER A LISTA DE LISTAS DE LINHAS COM CADA UMA DAS LINHAS DEMILITADAS POR VIRGULAS
    for(String s: lines){
        
        List <String> splitted = Arrays.asList(s.split(","));
        listSplittedLines.add(splitted);
        
    }
    
    
    for(int i =0; i<listSplittedLines.size(); i++ ){
        if(listSplittedLines.get(i).size() == 2){
            int pointId = Integer.parseInt(listSplittedLines.get(i).get(0));
            String pointName = listSplittedLines.get(i).get(1);
            
            this.addPoint(new Point (pointId, pointName));
            
            
        }else{
            if(listSplittedLines.get(i).size() == 8){
                int connectionId = Integer.parseInt(listSplittedLines.get(i).get(0));
                String type = listSplittedLines.get(i).get(1);
                
                String connectionName = listSplittedLines.get(i).get(2);
                int point1 = Integer.parseInt(listSplittedLines.get(i).get(3));
                int point2 = Integer.parseInt(listSplittedLines.get(i).get(4));
                boolean navigability;
                
                if(listSplittedLines.get(i).get(5).equals("true")){
                    navigability = true;
                }else{
                    navigability = false;
                }
                
                int cost = Integer.parseInt(listSplittedLines.get(i).get(6));
                int distance = Integer.parseInt(listSplittedLines.get(i).get(7));
                
                this.addConnection(point1,point2, new Connection (connectionId, type, connectionName,
                        point1, point2, navigability, cost, distance));
                        
            }
            
        }
        
    }
    }
    
    /**
     *
     * @return
     */
    @Override
    public String toString() {
        String phrase = "WALK PLANER ("+ diGraph.numVertices() +" connections | "+diGraph.numEdges()+" points)\n";
        List<Connection> connections;
        List<Connection> visitedBridges = new ArrayList<>();
        
        for(Vertex<Point> v1 : diGraph.vertices()){    
            
            
            for(Vertex<Point> v2 : diGraph.vertices()){
                if(v1 != v2){
                
                    
                    
                    connections = getConnectionsBetween(v1.element(),v2.element());
                    phrase += v1.element().toString() +" TO " + v2.element().toString() + "\n";
                    
                    if(!connections.isEmpty()){
                        for(Connection f : connections){
                            //verificar se connection existe
                            if(f.getType() == TYPE.BRIDGE && !visitedBridges.contains(f)){
                                visitedBridges.add(f);
                                phrase += f + "\n";
                            }
                                  //verificar se a conexao é ponte e ja foi visitada
                            if(f.getType() != TYPE.BRIDGE){
                                 phrase += f + "\n";
                            }
                          
                            
                          
                        }

                    }else{
                        phrase += "(no connections)\n\n";
                    }
                    phrase += "\n";
                }
            }
        }
        return phrase;
    }

    /**
     * Gets the point by the id of it
     * 
     * @param id
     * @return the point that has that id or null if it does not have
     */
    public Vertex<Point> getPointById(int id){
        for(Vertex<Point> p : diGraph.vertices()){
            if(p.element().getId()==id){
                return p;
            }
        }
        return null;
    }
  
    /**
     * Gets the connection by the id of it
     * 
     * @param id
     * @return the connection that has that id or null if it does not have
     */
    public Edge<Connection,Point> getConnectionById(int id){
        for(Edge<Connection,Point> edge : diGraph.edges()){
            if(edge.element().getId()==id){
                return edge;
            }
        }
        return null;
    }
    
    /**
     * Method that returns the desired path cost/distance  between two points (vertex), along with the edges it went through, by parameter
     * @param criteria
     * @param orig
     * @param dst
     * @param points
     * @param connections
     * @param usingBycicle
     * @return
     * @throws RoutePlannerException 
     */
    public double minimumCostPathWithConnections(Criteria criteria, Point orig, Point dst,
        List<Point> points, List<Connection> connections, boolean usingBycicle) throws RoutePlannerException {
        HashMap<Vertex<Point>, Vertex<Point>> parents = new HashMap();
        HashMap<Vertex<Point>, Double> distances = new HashMap();
        Vertex<Point> origin = checkPoint(orig);
        Vertex<Point> destination = checkPoint(dst);
        points.clear();

        if(usingBycicle){
            new StrategyCalcRouteBike().dijkstra(this, criteria, origin, distances, parents);
        }else{
            new StrategyCalcFoot().dijkstra(this, criteria, origin, distances, parents);
        }

        double cost = distances.get(destination);

        do{

            if(parents.get(destination) == null){
                throw new ErrorAlertException("Caminho Impossível!");


            }
            points.add(0, dst);
            if(getConnectionsBetween(destination.element(),parents.get(destination).element()).size() > 0){     
                    connections.add(getConnectionsBetween(destination.element(),parents.get(destination).element()).get(0));           
            }else if(getConnectionsBetween(parents.get(destination).element(), destination.element()).size() > 0){
                    connections.add(getConnectionsBetween(parents.get(destination).element(), destination.element()).get(0));  
            }
            destination = parents.get(destination);

        }while(destination != origin);

        return cost;
    }
    
    
    /**
     * 
     * returns all points of the graph into a list of points
     * 
     * @return points
     */
    public List<Point> getPoints(){
        List<Point> points = new ArrayList<>();
        for(Vertex<Point> p : this.diGraph.vertices()){
            points.add(p.element());
        }
        
        return points;
    }
    
    /**
     * 
     * returns all the edges of the graph into a list of connections
     * 
     * @return connections
     */
    public List<Connection> getConnections(){
        List<Connection> connections = new ArrayList<>();
        for(Edge<Connection, Point> p : this.diGraph.edges()){
            connections.add(p.element());
        }
        
        return connections;
    }
    
    /**
     * 
     * @param pointsToVisitList an observable list of the points to visit
     * @param criteria 
     * @param pointsToGet list of points we wanna get
     * @param travelledPaths list of the paths already travelled
     * @param isBycicle if it allows bycicle or not
     * 
     * @return the cost of the path
     */
    public double calcRoute(ObservableList<Point> pointsToVisitList, Criteria criteria,
            List <Point> pointsToGet, List<Connection> travelledPaths, boolean isBycicle){
        
        double objectiveCriteria = 0;
        for(int i = 0; i<pointsToVisitList.size()-1; i++){
            objectiveCriteria += minimumCostPathWithConnections(criteria, pointsToVisitList.get(i), 
                    pointsToVisitList.get(i+1), pointsToGet, travelledPaths, isBycicle);
        }
        objectiveCriteria += minimumCostPathWithConnections(criteria, pointsToVisitList.get(pointsToVisitList.size()-1),
                pointsToVisitList.get(0),  pointsToGet, travelledPaths, isBycicle);
        return objectiveCriteria;
    }

}

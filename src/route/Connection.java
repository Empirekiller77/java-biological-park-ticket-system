/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package route;

/**
 * Class that represents a connection in the graph
 * @author Henoch Vitureira and Pedro Lima
 */
public class Connection{

    /**
     * Enumerate the type of paths that can exist
     */
    public enum TYPE {
        PATH, 
        BRIDGE;

        private String getType() {
            switch(this) {
                case PATH: return "caminho";
                case BRIDGE: return "ponte";
            }
            return "Unknown";
        }
    };
    
    /*Attributes*/
    private int id;
    private String name;
    private boolean navigability;
    private int cost;
    private int distance;
    private int p1;
    private int p2;
    private TYPE type;
    private boolean selected;
    
    
     /**
     * inicializes the connection
     * 
     * @param id
     * @param type
     * @param name
     * @param idPoint1
     * @param idPoint2
     * @param navigability
     * @param cost
     * @param distance
     */
    public Connection(int id, String type, String name, int idPoint1, int idPoint2, 
            boolean navigability, int cost, int distance) {
        
        
        if(!(type.toLowerCase().equals("ponte")) && !(type.toLowerCase().equals("caminho")) ) 
            throw new IllegalArgumentException("O tipo só pode ser ponte ou caminho");
        
        if(cost < 0) throw new IllegalArgumentException("Preço tem que ser número positivo");
        if(distance <= 0) throw new IllegalArgumentException("Distância tem de ser maior que zero");
        
        if(type.toLowerCase().equals("ponte")){
            this.type = TYPE.BRIDGE;
        }else{
            this.type = TYPE.PATH;
        }
        
        this.id = id;
        this.name = name;
        this.navigability = navigability;
        this.cost = cost;
        this.distance = distance;
        
        this.p1 = idPoint1;
        this.p2 = idPoint2;
        this.selected = false;
    }
    
    

    /**
     * gets the id of the connection
     * 
     * @return the id
     */
    
    public int getId(){
        return this.id;
    }
    
    /**
     * gets the ty+e of the connection
     * 
     * @return the type
     */
    public TYPE getType(){
        return type;
    }
    
    /**
     * gets the name of the connection
     * 
     * @return the name
     */
    public String getName(){
        return this.name;
    }
    
    /**
     * gets the navigability of the connection
     * 
     * @return the navigability
     */
    public boolean getNavigability(){
        return this.navigability;
    }
    
    /**
     * gets the cost of the connection
     * 
     * @return the cost
     */
    public int getCost(){
        return this.cost;
    }
    
    /**
     * gets the distance of the connection
     * 
     * @return the distance
     */
    public int getDistance(){
        return this.distance;
    }
    
    /**
     * Shows the connection in string format
     * 
     * @return
     */
    @Override
    public String toString() {
        return String.format("{ID: " + getId()+ " | Tipo: " + getType() + " | Custo: " + getCost() + " | Distância: " + getDistance() + "}");
    }
}

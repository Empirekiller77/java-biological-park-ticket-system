package route;


import javafx.scene.control.Alert;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * class responsible for the exeception used in the route package
 * @author Henoch Vitureira and Pedro Lima
 */
public class ErrorAlertException extends RuntimeException{
    Alert alert;

    /**
     *Default Construct of the alert
     * @author
     */
    public ErrorAlertException() {
        super("Error");
        this.alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Erro!");
        alert.setHeaderText(null);
        alert.setContentText("An Error Ocurred!");
        this.alert.showAndWait();
        
    }

    /**
     *Construct of the alert with a custom message
     * @param message - the message to be shown
     */
    public ErrorAlertException(String message) {
        super(message);
        this.alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Erro!");
        alert.setHeaderText(null);
        alert.setContentText(message);
        this.alert.showAndWait();
    }
    
}

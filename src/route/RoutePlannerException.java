/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package route;

/**
 * class responsible for the handling the expections that involves routeManager 
 * @author Henoch Vitureira and Pedro Lima
 */
public class RoutePlannerException extends RuntimeException {

    /**
     *
     * @param string
     */
    public RoutePlannerException(String string) {
        super(string);
    }
    
}

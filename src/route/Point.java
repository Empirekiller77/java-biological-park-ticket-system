/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package route;

import java.io.Serializable;
import java.util.Objects;

/**
 * Class that represents a point in the graph
 * @author Henoch Vitureira and Pedro Lima
 */
public class Point implements Serializable
{
    //Atributes
    private int id;
    private String name;
    private boolean selected;
    
    /**
     * initialize point
     * 
     * @param id
     * @param name
     */
    public Point(int id, String name) {
        this.id = id;
        this.name = name;
    }
    /*Getters*/

    /**
     * gets the id of the point
     * 
     * @return the id of the point
     */
    public int getId(){
        return this.id;
    }
    
    /**
     * gets the name of the point
     * 
     * @return the name of the point
     */
    public String getName(){
        return this.name;
    }
    /*End of Getters*/
    
    /**
     * Shows the point in string format
     * 
     * @return the point in a string format
     */
    public String toString() {
        return getName() + "(" + getId() + ")";
    }
    
    /**
     * Compares two objects
     * 
     * @param obj
     * @return the logic value of the comparisation in the two objects
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Point other = (Point) obj;
        return this.name.compareToIgnoreCase(other.name) == 0;
    }

    /**
     *return a hashcode of a point
     * @return - an hash code
     */
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 83 * hash + Objects.hashCode(this.name);
        return hash;
    }
}

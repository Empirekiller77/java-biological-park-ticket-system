/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stats;

import route.Point;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * Class responsible for creating the window of statistic consulting
 * @author Lima
 */
public class GraphStats extends Stage{

    private final HBox hboxStatsBarChart;
    private final HBox hboxStatsPieChart;
    private final HashMap<Point, Integer> pointsStats;
    private final double TotalPrice;
    private final double nSoldOnFoot, nSoldOnBike, nTotalSold;
    
    /**
     * returns a new window of graphStats
     * @param pointsStats
     * @param statistcs 
     */
    public GraphStats(HashMap<Point, Integer> pointsStats, Stats statistcs){
        
        this.hboxStatsBarChart = new HBox();
        this.hboxStatsPieChart = new HBox();
        this.pointsStats = pointsStats;
        this.TotalPrice = statistcs.getTotalPrice();
        this.nSoldOnFoot = statistcs.getnSoldOnFoot();
        this.nSoldOnBike = statistcs.getnSoldOnBike();
        this.nTotalSold = statistcs.getnTotalSold();
        
        this.setTitle("Estatisticas");
        
        CategoryAxis xAxis = new CategoryAxis();
        xAxis.setLabel("Pontos");
        
        NumberAxis yAxis = new NumberAxis();
        yAxis.setLabel("Número de Visitas");
        
        BarChart barChart = new BarChart(xAxis, yAxis);
        
        XYChart.Series dataSeries1 = new XYChart.Series();
        dataSeries1.setName("Volume de Visitas");
        
        
        LinkedHashMap<Point, Integer> sortedPoints = sortHashMapByValues(this.pointsStats);
        
        for(Point p : sortedPoints.keySet()){
            dataSeries1.getData().add(new XYChart.Data(p.getName(), sortedPoints.get(p)));
        }
        Label lblMean;
        double mean = TotalPrice/nTotalSold;
        
        lblMean = new Label("Média do Preço dos Bilhetes Vendidos: " + String.valueOf((double)Math.round(mean * 100.0)/100.0) + "€");
        
        lblMean.setStyle("-fx-font-weight: bold; -fx-font-size: 18px;");
        
        barChart.getData().add(dataSeries1);
        barChart.setTitle("Pontos mais visitados");
        hboxStatsBarChart.getChildren().add(barChart);
        
        Scene scene = new Scene(new Group());//new Scene(vbox);
        /* ------------------------- Pie Chart ---------------------------*/
        double percentageFoot = ((nSoldOnFoot/nTotalSold)*100);
        double percentageBike = ((nSoldOnBike/nTotalSold)*100);
        ObservableList<PieChart.Data> pieChartData =
                FXCollections.observableArrayList(
                        
                        new PieChart.Data("A Pé: " + (double)Math.round(percentageFoot * 100.0)/100.0 + "%", percentageFoot),
                        new PieChart.Data("Bicicleta: " + (double)Math.round(percentageBike * 100.0)/100.0 + "%", percentageBike));
        final PieChart chart = new PieChart(pieChartData);
        
        chart.setTitle("Bilhetes vendidos para percursos a Pé e de Bicicleta");
        
        
        hboxStatsPieChart.getChildren().add(chart);
        
        VBox vbox = new VBox();
        vbox.setAlignment(Pos.CENTER);
        Separator separator1 = new Separator();
        separator1.setPadding(new Insets(15));
        
        Separator separator2 = new Separator();
        separator2.setPadding(new Insets(15));
        
        vbox.getChildren().addAll(hboxStatsBarChart, separator1, hboxStatsPieChart, separator2, lblMean);
        ((Group) scene.getRoot()).getChildren().add(vbox);
        this.getIcons().add(new Image("/icons/park.png"));
        this.setTitle("Estatisticas de Visitas");
        this.setScene(scene);
        this.setResizable(false);
        this.initModality(Modality.APPLICATION_MODAL);
        this.showAndWait();
    }
    
    /**
     * returns the parameter hashmap as an ordered and linkedhashmap
     * ordered by value of the hashmap
     * @param passedMap
     * @return 
     */
    public LinkedHashMap<Point, Integer> sortHashMapByValues(
        HashMap<Point, Integer> passedMap) {
        List<Point> mapKeys = new ArrayList<>(passedMap.keySet());
        List<Integer> mapValues = new ArrayList<>(passedMap.values());
        Collections.sort(mapValues);
        Collections.sort(mapKeys, Comparator.comparing(Point::getName));

        LinkedHashMap<Point, Integer> sortedMap =
            new LinkedHashMap<>();

        Iterator<Integer> valueIt = mapValues.iterator();
        while (valueIt.hasNext()) {
            Integer val = valueIt.next();
            Iterator<Point> keyIt = mapKeys.iterator();

            while (keyIt.hasNext()) {
                Point key = keyIt.next();
                Integer comp1 = passedMap.get(key);
                Integer comp2 = val;

                if (comp1.equals(comp2)) {
                    keyIt.remove();
                    sortedMap.put(key, val);
                    break;
                }
            }
        }
        return sortedMap;
    }
}

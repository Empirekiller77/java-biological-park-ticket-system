/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stats;

import java.io.Serializable;

/**
 * Class Responsible for the sold tickets statistics
 * @author Lima
 */
public class Stats implements Serializable{
    private double totalPrice, nSoldOnFoot, nSoldOnBike, nTotalSold;
    
    /**
     * initiates all the variables with 0
     */
    public Stats(){
        this.totalPrice = 0;
        this.nSoldOnFoot = 0;
        this.nSoldOnBike = 0;
        this.nTotalSold = 0;
    }
    
    /**
     * returns the totalPrice
     * @return totalPrice
     */
    public double getTotalPrice(){
        return this.totalPrice;
    }

    /**
     * returns the nSoldOnBike
     * @return nSoldOnBike
     */
    public double getnSoldOnBike() {
        return nSoldOnBike;
    }

    /**
     * returns the nSoldOnFoot
     * @return nSoldOnFoot
     */
    public double getnSoldOnFoot() {
        return nSoldOnFoot;
    }
    
    /**
     * returns the nTotalSold
     * @return nTotalSold
     */
    public double getnTotalSold() {
        return nTotalSold;
    }
    
    /**
     * For each ticket sold on bike increments the variable nSoldOnBike
     * @param cost, the cost of the ticket/path
     */
    public void sellBikeTicket(double cost){
        nSoldOnBike++;    
        sellTicket(cost);
    }
    
    /**
     * For each ticket sold on foot increments the variable nSoldOnFoot
     * @param cost, the cost of the ticket/path 
     */
     public void sellFootTicket(double cost){
        nSoldOnFoot++;    
        sellTicket(cost);
    }
    
    /**
     * For each ticket sold increments the variable nTotalSold
     * sums the current ticket cost to the totalPrice
     * @param cost, the cost of the ticket/path  
     */
    public void sellTicket(double cost){
        nTotalSold++;
        totalPrice += cost;
    }
    
   
}

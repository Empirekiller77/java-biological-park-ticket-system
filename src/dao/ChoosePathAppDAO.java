/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import view.ChoosePathApp;


/**
 * interface describing a DAO pattern method
 * @author Lima
 */
public interface ChoosePathAppDAO {

    /**
    *
    * @param app the ChoosePathApp to safe the info from
    */
    public void saveChoosePathApp(ChoosePathApp app);
    
    /**
     * 
     * inicializes ChoosePathApp with saved previously
     * 
     * @return one ChoosePathApp
     */
    public ChoosePathApp loadChoosePathApp();
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import route.ErrorAlertException;

/**
 * class responsible for the exeception used in the DAO pattern
 * @author Lima
 */
public class DAOException extends ErrorAlertException{

    /**
     *  the exception constructor
     * @param message the message to show
     */
    public DAOException(String message) {
        super(message);
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import route.Point;
import route.RouteManager;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import memento.VisitingPointsCareTaker;
import stats.Stats;
import view.ChoosePathApp;

/**
 *  Class responsible for serializing the necessary information of ChoosePathApp
 * @author Lima
 */
public class ChoosePathAppDAOSerialization implements ChoosePathAppDAO{
    //bilhetes -> cost, distancia, caminho, 
    private final String filename = "choosePathApp_save";

    /**
     *
     */
    public ChoosePathAppDAOSerialization() {
        
    }
    /**
    * 
    * the listOfinfo the list of the info to save (get(0) = caretaker, get(1) = stats
    * 
    * @param app so we can get all the information we need
    */
    @Override
    public void saveChoosePathApp(ChoosePathApp app) {
        try {
            FileOutputStream fout = new FileOutputStream(filename+".bin");

            ObjectOutputStream oos = new ObjectOutputStream(fout);
            ArrayList<Object> listOfInfo = new ArrayList();
                listOfInfo.add(app.getVisitsCaretaker());
                listOfInfo.add(app.getStats());
                listOfInfo.add(app.getMostVisitedPoints());
            oos.writeObject(listOfInfo);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * 
     * inicializes ChoosePathApp with saved previously
     * 
     * @return one ChoosePathApp
     */
    @Override
    public ChoosePathApp loadChoosePathApp() {
        try {
            FileInputStream fi = new FileInputStream(new File(filename+".bin"));
            ObjectInputStream oi = new ObjectInputStream(fi);
            
            // Read objects
            List<Object> infoList = (List<Object>) oi.readObject();
            
            Properties prop = new Properties();
            InputStream input = null;
            input = new FileInputStream("src/properties.properties");
            
            // load a properties file
            prop.load(input);
            
            //Load the map
            RouteManager manager = new RouteManager();
            manager.loadMap("src/"+prop.getProperty("Map"));
            
            //return the app with the saved info!
            ChoosePathApp newApp = new ChoosePathApp(manager, prop, (VisitingPointsCareTaker)infoList.get(0),
                    (Stats) infoList.get(1), (HashMap<Point,Integer>) infoList.get(2));
            return newApp;

        } catch (IOException e) {
            throw new DAOException ("Não Existe Informação Armazenada!");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ChoosePathAppDAOSerialization.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
}

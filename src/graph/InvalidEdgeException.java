/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graph;

/**
 * class responsible for the handling the expections that involve edges
 * @author Henoch Vitureira and Pedro Lima
 */
public class InvalidEdgeException extends RuntimeException {

    /**
     *
     */
    public InvalidEdgeException() {
        super("The edge is invalid or does not belong to this graph.");
    }

    /**
     *
     * @param string
     */
    public InvalidEdgeException(String string) {
        super(string);
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Singleton;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Date;

/**
 * Class responsible for logging the actions in the Application, based ont he properties file
 * @author Henoch
 */
public class ActionLogger {
    /**
     * Creates a file if it doesn't exist that logs the action in the app to a file
     * @param log
     * @throws IOException 
     */
    public void logAction(String log) throws IOException{
        File newLogFile = new File("Log.txt");
        newLogFile.createNewFile(); // if file already exists will do nothing
        FileOutputStream logFile = new FileOutputStream(newLogFile, true); 
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(logFile));
        bw.append(System.lineSeparator() + log + "| Data: " + new Date());
        bw.close();
        logFile.close();
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Singleton;
import java.io.IOException;

/**
 * Class responsible for being the singleton of the action logger
 * @author
 */
public final class ActionLoggerSingleton {
    
    private static ActionLoggerSingleton instance = new ActionLoggerSingleton();

    ActionLogger logger;
    
    private ActionLoggerSingleton() {
        logger = new ActionLogger();
    }
    
    /**
     * returns the unique instance of thhe singleton
     * @return 
     */
    public static ActionLoggerSingleton getInstance()   {
        return instance;
    }
    
    /**
     * Invokes que ActionLogger logging method
     * @param log
     * @throws IOException 
     */
    public void log(String log) throws IOException{
        logger.logAction(log);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package templateMethod;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;

/**
 * Class responsible for printing the PDF fiel fo the bill and ticket
 * @author Henoch
 */
public abstract class GeneratePDF {
    
    /**
     * Static method to print the PDF fiel fo the bill and ticket
     * @param bill the bill to print from
     * @throws FileNotFoundException - thrown when the file does nto exist
     * @throws DocumentException - itext exception
     */
    public static void GeneratePDF(Bill bill) throws FileNotFoundException, DocumentException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy 'T' HH-mm-ss") ;
        
        //Bill
        OutputStream billFile = new FileOutputStream(new File(bill.getContribuinte()+ "_" + dateFormat.format(bill.getData()) + "_fatura.pdf"));
        bill.printFile(bill, billFile);
        
        //Ticket
        OutputStream ticketFile = new FileOutputStream(new File(bill.getContribuinte()+ "_" + dateFormat.format(bill.getData()) + "_bilhete.pdf"));
        bill.printTicketFile(bill, ticketFile);  
    }
    
    /**
     *
     * @param bill the bill to print the info from
     * @param file the file to print on
     * @throws DocumentException itext exception
     */
    
    public abstract void printFile(Bill bill, OutputStream file) throws DocumentException;
    
    
    /**
     * Writes the header of the document
     * @param bill the bill to print the info from
     * @param doc the document to write on
     * @throws DocumentException 
     */
    public void header(Bill bill, Document doc) throws DocumentException{
    }
    
    /**
     * Writes the footer of the document
     * @param bill the bill to print the info from
     * @param doc the document to write on
     * @throws DocumentException 
     */
    public void footer(Bill bill, Document doc) throws DocumentException{
    }
}

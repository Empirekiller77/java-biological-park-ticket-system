/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package templateMethod;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import route.Connection;
import route.Point;


/**
 * Class responsible for managing the tickets sold
 * @author Henoch
 */
public class Ticket extends GeneratePDF{
    
    private final Date date;   
    private final double cost;
    private final double distance;
    private List<Point> visitedPoints = null;
    private List<Connection> travelledPaths;
    private final String tripType;
   
    /**
     * instanciate the ticket
     * @param date the date of the ticket
     * @param cost the cost of the ticket
     * @param distance the distance travelled of the ticket
     * @param visitedPoints the visited points of the trip
     * @param travelledPaths the crossed paths of the trip
     * @param tripType - the trip type, foot or bike
     */
    protected Ticket(Date date, double cost, double distance, List<Point> visitedPoints, List<Connection> travelledPaths, String tripType) {      
        this.date = date;
        this.cost = cost;
        this.distance = distance;
        this.visitedPoints = visitedPoints;
        this.travelledPaths = travelledPaths;
        this.tripType = tripType;
    }
    
    /**
     * returns the Date of the ticket
     * @return date the ticket's date
     */
    public Date getData() {
        return date;
    }
    
    /**
     * returns the cost of the ticket
     * @return cost the ticket's cost
     */
    public double getCost() {
        return cost;
    }
    
    /**
     * return the distance of the path of the ticket
     * @return distance
     */
    public double getDistance() {
        return distance;
    }

    /**
     * returns the list of points visited
     * @return visitedPoins
     */
    public List<Point> getVisitedPoints() {
        return visitedPoints;
    }
    
    /**
     * Returns a string with the trip type
     * @return the type, as a string
     */
    public String getTripType() {
        return tripType;
    }
    
    /**
     * returns the connections that were used in the choosen path
     * @return travelledPaths
     */
    public List<Connection> getTravelledPaths() {
        return travelledPaths;
    }

    /**
     * Responsible for printing the ticket witht he template method pattern, receiving the bill of the ticket to print
     * @param bill the current bill
     * @param file the file to print on
     * @throws DocumentException itext exception
     */
    @Override
    public void printFile(Bill bill, OutputStream file) throws DocumentException {
        Document ticket = new Document();
        PdfWriter.getInstance(ticket, file);
        ticket.open();
        ticket.add(new Paragraph("BILHETE DE ENTRADA"));
        header(bill, ticket);
        ticket.add(new Paragraph("\nPontos Visitados : "));

        for(Point point: bill.getVisitedPoints()){
            ticket.add(new Paragraph("     "+point));
        }
        ticket.add(new Paragraph("\nCaminhos/Pontes Percorridos : "));

        for(Connection c: sortTravelledPaths(bill.getTravelledPaths())){
            ticket.add(new Paragraph("     "+c));
        }

        footer(bill, ticket);
        ticket.close();
    }
    
    /**
     * sorts the paths that the user travelled
     * @param travelledPaths list of paths travelled by the user
     * @return  the paths sorted that the user travelled
     */
    public List<Connection> sortTravelledPaths(List<Connection> travelledPaths){
        List<Connection> travelledPathsSorted = new ArrayList<>();
        
        for(Connection c : travelledPaths){
            if(!travelledPathsSorted.contains(c) ){
              travelledPathsSorted.add(c);
            }
            
        }
        
        return travelledPathsSorted;
    }
    
    /**
     * Writes the header of the document
     * @param bill the bill to print the info from
     * @param doc the document to write on
     * @throws DocumentException 
     */
    @Override
    public void header(Bill bill, Document doc) throws DocumentException{
        doc.add(new Paragraph("Data : "+ bill.getData()));
        doc.add(new Paragraph("Tipo de Percuso : "+ bill.getTripType()));
    }
    /**
     * Writes the footer of the document
     * @param bill the bill to print the info from
     * @param doc the document to write on
     * @throws DocumentException 
     */
    @Override
    public void footer(Bill bill, Document doc) throws DocumentException{
        doc.add(new Paragraph("\nCusto : "+ bill.getCost() + "€"));
        doc.add(new Paragraph("Distância : "+ bill.getDistance()));
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package templateMethod;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import route.Connection;
import route.Point;
import java.util.Date;
import java.util.List;
import java.io.OutputStream;

/**
 * Class responsible for managing the bills
 * @author Henoch
 */
public class Bill extends Ticket{
    private static int NUMERO_FATURA = 1;
    private final int numero;
    private final String contribuinte;
    private List<Connection> travelledPaths;
   
    /**
     * inicializes the Bill
     * 
     * @param contribuinte the tax payer number
     * @param date the current date
     * @param cost the cost of the bill
     * @param distance the distance travelled
     * @param visitedPoints the visited points
     * @param travelledPaths the pathj that were crossed
     */
    private Bill(String contribuinte, Date date, double cost, double distance, List<Point> visitedPoints, List<Connection> travelledPaths, String tripType) {
            super(date, cost, distance, visitedPoints, travelledPaths, tripType);
            this.contribuinte = contribuinte;        
            this.numero = NUMERO_FATURA++; 
    }
    
    /**
     * 
     * returns the tax payer number of the bill
     * 
     * @return contribuinte - the tax payer number
     */
    public String getContribuinte() {
        return contribuinte;
    }

    /**
     * 
     * returns the number of the bill
     * 
     * @return the number
     */
    public int getNumero() {
        return numero;
    }
   
    /**
     * returns a new Bill with text payer number  
     * @param contribuinte the tax payer number
     * @param cost the cost of the bill
     * @param distance the distance travelled
     * @param visitedPoints the visited points
     * @param travelledPaths the paths crossed
     * @param tripType - the type of trip foot/bike
     * @return a new Bill without tax payer number
     */
    public static Bill comContribuiente(String contribuinte, double cost, double distance,
            List<Point> visitedPoints, List<Connection> travelledPaths, String tripType){
        return new Bill(contribuinte, new Date(), cost, distance, visitedPoints, travelledPaths, tripType);
    }
    
    /**
     * returns a new Bill without text payer number  
     * @param cost the cost of the bill
     * @param distance the distance travelled
     * @param visitedPoints the visited points
     * @param travelledPaths the paths crossed
     * @param tripType - the type of trip foot/bike
     * @return a new Bill with tax payer number
     */
    public static Bill semContribuiente(double cost, double distance, List<Point> visitedPoints,
            List<Connection> travelledPaths, String tripType){
        return new Bill("999999999", new Date(), cost, distance, visitedPoints, travelledPaths, tripType);
    }
    
    /**
     * prints a bill into a PDF file
     * @param bill the bill to print
     * @param file the file to print on
     * @throws DocumentException the exception thrown
     */
    @Override
    public void printFile(Bill bill, OutputStream file) throws DocumentException {
        Document billing = new Document();
        PdfWriter.getInstance(billing, file);
        billing.open();
        billing.add(new Paragraph("Numero de Contribuinte: "+ bill.getContribuinte()));
        header(bill, billing);
        footer(bill, billing);
        billing.close();
    }
    
    /**
     * prints the ticket of wich the bill corresponds to
     * @param bill the current bill
     * @param file the file to print on
     * @throws DocumentException trhe exception thrown
     */
    public void printTicketFile(Bill bill, OutputStream file) throws DocumentException {
        super.printFile(bill, file);
    }
}

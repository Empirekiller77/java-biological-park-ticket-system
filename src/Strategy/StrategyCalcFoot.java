/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Strategy;

import graph.Edge;
import graph.Vertex;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import route.Connection;
import route.Point;
import route.RouteManager;

/**
 *Class responsible for being the concrete strategy to calculate a path between two points, by foot
 * @author Henoch
 */
public class StrategyCalcFoot extends StrategyRoute implements StrategyRouteCalc{


    @Override
    public void dijkstra(RouteManager rm, RouteManager.Criteria criteria, Vertex<Point> orig, Map<Vertex<Point>, Double> costs, Map<Vertex<Point>, Vertex<Point>> predecessors) {
  
    Set<Vertex<Point>> unvisited;
        unvisited = new HashSet();
        Iterable<Vertex<Point>> vertexes = rm.getDiGraph().vertices();
    
        for(Vertex<Point> vertex : vertexes){
            unvisited.add(vertex);
            costs.put(vertex, Double.MAX_VALUE);
            predecessors.put(vertex, null);
        }
        costs.put(orig,0.0);
        while(!unvisited.isEmpty()){
            Vertex<Point> lowCostVert = findLowerCostVertex(unvisited, costs);
            unvisited.remove(lowCostVert);
                Iterable<Edge<Connection,Point>> accedeentEdges = rm.getDiGraph().accedentEdges(lowCostVert);
            for(Edge<Connection,Point> edge : accedeentEdges){

                Vertex<Point> opposite = rm.getDiGraph().opposite(lowCostVert, edge);
               
                if(unvisited.contains(opposite)){
                    double dist;
                    if(criteria == RouteManager.Criteria.COST){
                        dist = edge.element().getCost()+ costs.get(lowCostVert);
                    }else{
                        dist = edge.element().getDistance()+ costs.get(lowCostVert);
                    }
                    
                    if(costs.get(opposite) > dist){
                        costs.put(opposite, dist);
                        predecessors.put(opposite, lowCostVert);
                    }
                }
            }
        }  
    }


   
    
}

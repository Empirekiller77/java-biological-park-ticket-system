/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Strategy;

import graph.Vertex;
import java.util.Map;
import java.util.Set;
import route.Point;

/**
 *Abstract class responsible for sharing the method to find the lower cost vertex, an aux method for the strategies
 * @author Henoch
 */
public abstract class StrategyRoute {
    /**
     * helps find the vertex that has the lowest cost
     * 
     * @param unvisited
     * @param costs
     * @return the vertex with minimum cost
     */
    public Vertex<Point> findLowerCostVertex(Set<Vertex<Point>> unvisited, 
            Map<Vertex<Point>,Double> costs) {
        
        double min = Double.MAX_VALUE;
        Vertex<Point> minCostVertex = null;
        for(Vertex<Point> vertex : unvisited){
            if(costs.get(vertex) <= min){
                minCostVertex = vertex;
                min = costs.get(vertex);
            }
        }
        return minCostVertex;
    }
    
    
}

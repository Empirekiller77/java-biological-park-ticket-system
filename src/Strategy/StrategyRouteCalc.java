/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Strategy;

import graph.Vertex;
import java.util.Map;
import route.Point;
import route.RouteManager;

/**
 * Interface responsible for defining the behavior of the concrete strategies for calculating the path between two points
 * @author Henoch
 */
public interface StrategyRouteCalc {
   
    /**
     *
     * @param rm - the route manager
     * @param criteria - the chosen criteria (cost/distance)
     * @param orig - the origin point
     * @param costs - the distances/costs
     * @param predecessors - the predecessors of vertexes
     */
    public void dijkstra(RouteManager rm, RouteManager.Criteria criteria, Vertex<Point> orig, Map<Vertex<Point>, 
            Double> costs, Map<Vertex<Point>, Vertex<Point>> predecessors);
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.io.IOException;
import javafx.application.Application;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import view.Load;

/**
 *
 * @author Lima and Henoch
 */
public class Main extends Application {
    
    
    @Override
    public void start(Stage primaryStage) throws IOException {
        primaryStage = new Load();
        primaryStage.setTitle("Carregar Informação");
        primaryStage.getIcons().add(new Image("/icons/park.png"));
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
    
    
}
